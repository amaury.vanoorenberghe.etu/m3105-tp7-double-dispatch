package fr.univlille.iutinfo.m3105.ex3;

import java.util.Collection;

public abstract class Throwable {
	public abstract Collection<Integer> get();
	
	public abstract Throwable combine(Throwable other);
}
