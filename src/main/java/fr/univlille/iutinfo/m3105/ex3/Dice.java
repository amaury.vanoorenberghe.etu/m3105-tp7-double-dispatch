package fr.univlille.iutinfo.m3105.ex3;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

public class Dice extends Throwable {
	private static final Random RNG = new Random();
	
	private final int FACES;
	
	public Dice(int faces) {
		if (faces <= 0) {
			throw new InvalidParameterException();
		}
		FACES = faces;
	}
	
	public Collection<Integer> get() {
		return Arrays.asList(1 + RNG.nextInt(FACES));
	}
	
	public int getFaces() {
		return FACES;
	}

	@Override
	public Throwable combine(Throwable other) {
		return new HandfulOfDices(this, other);
	}
}