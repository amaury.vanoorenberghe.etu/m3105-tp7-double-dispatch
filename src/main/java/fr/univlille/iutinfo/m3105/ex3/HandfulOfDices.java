package fr.univlille.iutinfo.m3105.ex3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class HandfulOfDices extends Throwable {
	private final List<Throwable> throwables;
	
	public HandfulOfDices(Throwable a, Throwable... b) {
		this(a);
		throwables.addAll(Arrays.asList(b));
	}
	
	public HandfulOfDices(Throwable a) {
		throwables = Arrays.asList(a);
	}
	
	@Override
	public Collection<Integer> get() {
		List<Integer> result = new ArrayList<>();
		
		for (Throwable t : throwables) {
			result.addAll(t.get());
		}
		
		return result;
	}

	@Override
	public Throwable combine(Throwable other) {
		//ITEMS.add(other);
		//return this;
		return new HandfulOfDices(other, throwables.toArray(Throwable[]::new));
	}
}