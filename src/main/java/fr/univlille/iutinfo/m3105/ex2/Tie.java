package fr.univlille.iutinfo.m3105.ex2;

public class Tie extends ShiFuMi {
	public static final ShiFuMi INSTANCE = new Tie();
	
	private Tie() {}
	
	@Override
	public ShiFuMi against(ShiFuMi other) {
		return this;
	}

	@Override
	public ShiFuMi againstRock(ShiFuMi other) {
		return this;
	}

	@Override
	public ShiFuMi againstPaper(ShiFuMi other) {
		return this;
	}

	@Override
	public ShiFuMi againstScissors(ShiFuMi other) {
		return this;
	}

	@Override
	public String toString() {
		return "Égalité";
	}
}