package fr.univlille.iutinfo.m3105.ex2;

public class Rock extends ShiFuMi {
	@Override
	public ShiFuMi against(ShiFuMi other) {
		return other.againstRock(this);
	}

	@Override
	public ShiFuMi againstRock(ShiFuMi other) {
		return TIE;
	}

	@Override
	public ShiFuMi againstPaper(ShiFuMi other) {
		return other;
	}

	@Override
	public ShiFuMi againstScissors(ShiFuMi other) {
		return this;
	}

	@Override
	public String toString() {
		return "Pierre";
	}
}