package fr.univlille.iutinfo.m3105.ex2;

public class Paper extends ShiFuMi {
	@Override
	public ShiFuMi against(ShiFuMi other) {
		return other.againstPaper(this);
	}

	@Override
	public ShiFuMi againstRock(ShiFuMi other) {
		return this;
	}

	@Override
	public ShiFuMi againstPaper(ShiFuMi other) {
		return TIE;
	}

	@Override
	public ShiFuMi againstScissors(ShiFuMi other) {
		return other;
	}

	@Override
	public String toString() {
		return "Feuille";
	}
}
