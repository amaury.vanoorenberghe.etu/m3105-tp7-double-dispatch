package fr.univlille.iutinfo.m3105.ex2;

import java.util.Random;
import java.util.function.Supplier;

public enum Factory {
	ROCK(Rock::new),
	PAPER(Paper::new),
	SCISSORS(Scissors::new);
	
	private final Supplier<ShiFuMi> CONSTRUCTOR;
	
	private Factory(Supplier<ShiFuMi> constructor) {
		CONSTRUCTOR = constructor;
	}
	
	public ShiFuMi create() {
		return CONSTRUCTOR.get();
	}
	
	public static ShiFuMi random() {
		final Factory[] tab = values();
		return tab[new Random().nextInt(tab.length)].create();
	}
}
