package fr.univlille.iutinfo.m3105.ex2;

/**
 * Rock, Paper, Scissors
 */
public abstract class ShiFuMi {
	public static final ShiFuMi TIE = Tie.INSTANCE;
	
	public abstract ShiFuMi against(ShiFuMi other);

	public abstract ShiFuMi againstRock(ShiFuMi other);

	public abstract ShiFuMi againstPaper(ShiFuMi other);

	public abstract ShiFuMi againstScissors(ShiFuMi other);
	
	@Override
	public abstract String toString();
}