package fr.univlille.iutinfo.m3105.ex2;

public class Scissors extends ShiFuMi {
	@Override
	public ShiFuMi against(ShiFuMi other) {
		return other.againstScissors(this);
	}

	@Override
	public ShiFuMi againstRock(ShiFuMi other) {
		return other;
	}

	@Override
	public ShiFuMi againstPaper(ShiFuMi other) {
		return this;
	}

	@Override
	public ShiFuMi againstScissors(ShiFuMi other) {
		return TIE;
	}

	@Override
	public String toString() {
		return "Sciseaux";
	}
}
