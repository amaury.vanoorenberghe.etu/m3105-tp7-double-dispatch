package fr.univlille.iutinfo.m3105.ex1;

public class Fahrenheit extends Scale {
	public static final double OFFSET = 459.67;
	public static final double FACTOR = 9.0 / 5.0;
	
	@Override
	public String getName() {
		return "Fahrenheit";
	}

	@Override
	public String getAbbrev() {
		return "F";
	}

	@Override
	public double toKelvin(double fahrenheit) {
		return (fahrenheit + OFFSET) / FACTOR;
	}

	@Override
	public double fromKelvin(double kelvin) {
		return kelvin * FACTOR - OFFSET;
	}
}