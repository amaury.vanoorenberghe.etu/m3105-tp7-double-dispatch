package fr.univlille.iutinfo.m3105.ex1;

public abstract class Scale {
	public abstract String getName();
	public abstract String getAbbrev();
	public abstract double toKelvin(double value);
	public abstract double fromKelvin(double value);
}