package fr.univlille.iutinfo.m3105.ex1;

public class Celsius extends Scale {
	public static final double OFFSET = 273.15;
	
	@Override
	public String getName() {
		return "Celsius";
	}

	@Override
	public String getAbbrev() {
		return "C";
	}

	@Override
	public double toKelvin(double celsius) {
		return celsius + OFFSET;
	}

	@Override
	public double fromKelvin(double kelvin) {
		return kelvin - OFFSET;
	}
}
