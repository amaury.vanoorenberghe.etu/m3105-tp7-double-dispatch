package fr.univlille.iutinfo.m3105.ex1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.Test;

public class ScaleTest {
	public static final double DELTA = 1E-7;
	public static final Scale CELSIUS = new Celsius();
	public static final Scale FAHRENHEIT = new Fahrenheit();
	
	@Test
	protected void name_should_be_correct() {
		assertEquals("Celsius", CELSIUS.getName());
		assertEquals("Fahrenheit", FAHRENHEIT.getName());
	}

	@Test
	protected void abbrev_should_be_correct() {
		assertEquals("C", CELSIUS.getAbbrev());
		assertEquals("F", FAHRENHEIT.getAbbrev());
	}
	
	@Test
	protected void to_kelvin_returns_correct_value() {
		assertEquals(273.15, CELSIUS.toKelvin(0));
		assertEquals(0, CELSIUS.toKelvin(-273.15));
		
		assertEquals(260.92777778, FAHRENHEIT.toKelvin(10), DELTA);
		assertEquals(249.81666667, FAHRENHEIT.toKelvin(-10), DELTA);
	}
	
	@Test
	protected void from_kelvin_returns_correct_value() {
		assertEquals(-273.15, CELSIUS.fromKelvin(0));
		assertEquals(0, CELSIUS.fromKelvin(273.15));
		
		assertEquals(-441.67, FAHRENHEIT.fromKelvin(10), DELTA);
		assertEquals(-477.67, FAHRENHEIT.fromKelvin(-10), DELTA);
	}
	
	@Test
	protected void converting_back_and_forth_returns_initial_value() {
		final double i = new Random().nextDouble();
		final String comment = String.format("i = %f", i);
		
		assertEquals(i, CELSIUS.fromKelvin(CELSIUS.toKelvin(i)), DELTA, comment);
		assertEquals(i, CELSIUS.toKelvin(CELSIUS.fromKelvin(i)), DELTA, comment);
		assertEquals(i, FAHRENHEIT.fromKelvin(FAHRENHEIT.toKelvin(i)), DELTA, comment);
		assertEquals(i, FAHRENHEIT.toKelvin(FAHRENHEIT.fromKelvin(i)), DELTA, comment);
	}
}
