package fr.univlille.iutinfo.m3105.ex2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ShiFuMiTest {
	public static final ShiFuMi 
	ROCK = new Rock(),
	PAPER = new Paper(),
	SCISSORS = new Scissors(),
	TIE = ShiFuMi.TIE;
	
	@Test
	protected void proper_gesture_should_win() {
		assertEquals(TIE, 		ROCK.against(ROCK));
		assertEquals(PAPER, 	ROCK.against(PAPER));
		assertEquals(ROCK, 		ROCK.against(SCISSORS));
		
		assertEquals(PAPER, 	PAPER.against(ROCK));
		assertEquals(TIE, 		PAPER.against(PAPER));
		assertEquals(SCISSORS, 	PAPER.against(SCISSORS));
		
		assertEquals(ROCK, 		SCISSORS.against(ROCK));
		assertEquals(SCISSORS, 	SCISSORS.against(PAPER));
		assertEquals(TIE, 		SCISSORS.against(SCISSORS));
		
		assertEquals(TIE,		ROCK.against(TIE));
		assertEquals(TIE,		PAPER.against(TIE));
		assertEquals(TIE,		SCISSORS.against(TIE));
		assertEquals(TIE,		TIE.against(TIE));
	}
}